%% AB1 de Controle 2
% Larissa Artemis Luna Monteiro - 13112514
% Engenharia da Computa��o - IC - UFAL

%% Quest�o 1

num = [25.04 5.008];
den = [1 5.03247 25.1026 5.008];
n = 3 % ordem do sistema

[A, B, C, D] = tf2ss(num, den);

P = [0 0 1; 0 1 0; 1 0 0];
A = inv(P)*A*P;
B = inv(P)*B;
C = C*P;

cm = [B A*B A^2*B]; % matriz de controlabilidade
rcm = rank(cm); % rank da matriz de controlabilidade

if (n == rcm)
    disp('O sistema � control�vel')
else
    disp('O sistema *n�o* � control�vel')
end

om = [C C*A C*A^2]'; % matriz de observabilidade
rom = rank(cm); % rank da matriz de controlabilidade

if (n == rom)
    disp('O sistema � observ�vel')
else
    disp('O sistema *n�o* � observ�vel')
end

%% Quest�o 2

% o sistema � descrito por y''' + 6y'' + 11y' + 6y = 6u
% fazendo a transformada de laplace com condi��es iniciais iguais a zero,
% obtemos a fun��o de transfer�ncia do sistema
num = [6];
den = [1 6 11 6];
disp('A fun��o de transfer�ncia do sistema �')
t = tf(num, den)

[A, B, C, D] = tf2ss(num, den);
P = [0 0 1; 0 1 0; 1 0 0];
disp('A representa��o em espa�o de estados do sistema �')
A = inv(P)*A*P
B = inv(P)*B
C = C*P

disp('A representa��o em diagonal do sistema �')
td = canon(t,'modal')

%% Quest�o 5
num = [1];
den = [1 3];

[A, B, C, D] = tf2ss (num, den)
J = [-5]
acker(A, B, J)

